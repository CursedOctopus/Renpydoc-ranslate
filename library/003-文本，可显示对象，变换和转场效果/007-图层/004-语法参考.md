## 语法参考
请注意在if语句中，当分层图片第一次被定义时所有条件语句表达将会在初始化时生效。    

### Layeredimage
`layeredimage`语句是Ren'py脚本语言中引入分层图片的语句。它以图像名起始，并包含一个语句块，里面有属性、组和if语句。    

Layeredimage 有以下特性：    

__image_format__    
当一个图像是一串字符的时候， and this is supplied, 图像名会被插入 image_format 来形成一个图像文件。 比如说，"sprites/eileen/{image}.png" 将会在sprites子文件夹下寻找图像。(This is not used by auto groups, which look for images and not image files.)    

__format_function__    
这是一种代替 layeredimage.format_funtion的方法，用来格式化图像信息使之成为一个可显示对象。    

__transform properties__     
如果出现，则表示引入了一个应用于可显示对象的 transform()语句。    

__at__          
将一个变换或一组变换作用于分层图像。    

### 属性 Attribute
`attribute`语句将一层带有该属性的图层加到原有的图像上。相同属性可以被用在多个图层上，所有和该属性相关的图层会被显示（if_also和if_not特性可以改变它）。    

一个属性拥有一个属性名。它可以带两个关键字。“default”关键字表示此属性在该组中没有相同属性的情况下，将会被显示出来。`null`关键字防止Ren'py自动搜索关于此属性的可显示对象，比如在一个属性被if_all, if_any,if_not单独使用时可以用到null关键字。   

如果可显示对象没有声明，它将会由图层，组，组变形和属性名计算而来，所有空格会被替换为下划线并会将它们组合起来。如果我们有一个名为“augustina”的图像，组名“eyes”和属性名“closed”将会被组合成“augustina_eyes_closed”。（分层图片的format function会被用来完成这一功能，默认是 layeredimage.format_funtion()。）    

如果一个属性没有在组里，it's placed in a group with the same name, but that group is not used to compute the displayable name. (所以将会搜索 "image_attribute", 而不是 "image_attribute_attribute").    

attribute 语句有以下几种特性：    

__if_all__    
一串或一系列字符串组成的属性名。如果申明了此特性，此层只在所有属性名被声明时才显示。    

__if_any__    
一串或一系列字符串组成的属性名。如果申明了此特性，此层在任何其中之一的属性名被声明时才显示。     

__if_not__    
一串或一系列字符串组成的属性名。如果申明了此特性，此层在任何其中之一的属性名都不被声明时才显示。

__transform properties__     
如果出现，则表示引入了一个应用于图层的 transform()语句。    

__at__     
将一个变换或一组变换作用于图层。

### 组 Group
`group`语句将所有可选的图层组合起来。当一个属性在组里时，it is an error to include any of the other attributes in that group. (But it's fine to include the same attribute twice.)    

group语句后跟一个名字。这个名字不会被经常用到，不过会被用来产生组里属性的默认名。    

名字可以跟auto关键字。当auto关键字出现时，在组里的任何属性被声明之后，Ren'py会扫描它的图像列表，来寻找能匹配上组格式（见下面描述）的项目。所有被发现没有和现有属性对应上的图像将会被加入到组里。    

可以在组的第一行中声明特性，组会跟着一个包含特性和属性的语句块。    

这里有两个可以定义组的特性：    

__variant__    
如果给出variant，它应该是一串字符。variant会给group加上一个变种元素，使之成为自动生成图像名和用来搜索自动定义属性组格式的一部分。   

__prefix__     
如果给出prefix，前缀和自动或手动命名的属性名用下划线连接。如果前缀是“leftarm”，遇到属性名“hip”，那么新的属性名“leftarm_hip”将会被重新定义。

group语句也接受和`attribute`的特性相同的参数。组的特性会传递给组内的属性，除非被属性自己的（相同）特性覆盖。    

__Pattern.__ 图像格式包括：    

 * 图像名称，空格由下划线代替
 * 组名称
 * 变种名称
 * 属性名称    

 所有都由下划线来组合。比如，有一个分层图片，名为“augustina work”，组名为"eyes"，它将会匹配图像，图像要匹配格式 augustina_work_eye_`attribute`. 如果有变种名blue，匹配的格式为 augustina_work_eye_blue_`attribute`。    

### Always
`always`语句表示一个总是显示的图层。 它必须作用在可显示对象上，并可以带上特性。可以在一行中写或跟一个语句块。    

always语句有以下特性：    

__if_all__    
一串或一系列字符串组成的属性名。如果申明了此特性，此层只在所有属性名被声明时才显示。    

__if_any__         
一串或一系列字符串组成的属性名。如果申明了此特性，此层在任何其中之一的属性名被声明时才显示。     

__if_not__    
一串或一系列字符串组成的属性名。如果申明了此特性，此层在任何其中之一的属性名都不被声明时才显示。

__transform properties__      
如果出现，则表示引入了一个应用于图层的 transform()语句。    

__at__         
将一个变换或一组变换作用于图层。   

### If
`if`语句（完全形态是 if-elif-else）可使你创建一个或多个用来判断的条件语句。每一层关联一个条件，第一个为真的条件首先显示。如果没有条件为真，else层（如果存在的话）会显示。  

一个比较完整的例子可能张这样：  
```python
if glasses == "evil":
    "augustina_glasses_evil"
elif glasses == "normal":
    "augustina_glasses"
else:
    "augustina_nose_mark"
```
每一层必须有一个可显示对象。也可以加下面的特性：    

__if_all__    
一串或一系列字符串组成的属性名。如果申明了此特性，此层只在所有属性名被声明时才显示。    

__if_any__         
一串或一系列字符串组成的属性名。如果申明了此特性，此层在任何其中之一的属性名被声明时才显示。     

__if_not__    
一串或一系列字符串组成的属性名。如果申明了此特性，此层在任何其中之一的属性名都不被声明时才显示。

__transform properties__    
如果出现，则表示引入了一个应用于图层的 transform()语句。    

__at__        
将一个变换或一组变换作用于图层。   

if语句在`layeredimage`语句运行时会转化为 ConditionSwitch()。    

当 predict_all 不为真，分层图片正在显示或将要显示时，需要避免if语句条件的改变。 因为可能会导致不可预测的图像加载。 It's intended for use for character customization options that change rarely.   
