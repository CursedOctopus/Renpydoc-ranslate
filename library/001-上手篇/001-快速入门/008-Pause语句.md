## Pause 语句
Pause命令会使 Ren'Py 系统暂停，直到玩家按下鼠标为止。
```python
pause
```
如果给出了数字参数，系统将会以秒为单位暂停。
```python
pause 3.0
```
