# renpy-wiki-translate

<br>

本wiki框架由[amWiki](http://amwiki.org/)提供，感谢amWiki作者。     
## Ren'Py电子小说引擎
Ren'Py 是一个创建电子小说（Visual Novel或ADV游戏）的引擎。所谓电子小说是指一种以电脑为媒介，融合文字、背景、人物图片、及声音的多媒体故事叙述方式。Ren'Py 支持一种类似电影脚本的写作方法，这样会使制作一个简单的游戏变得相对轻松，同时高级开发者也能根据需要进行自定义和扩展(使用Python语言)。游戏制作者不需要额外的工作就可以使用作为电子小说所需要大部分功能，比如说像储存、读取、选项设置以及回顾等。最后，Ren'Py 支持 Windows，Mac OS X，和 Linux x86，而且还能在其它平台上运行。    

Ren'Py 是一个开源软件，可以免费地用于各种商业或非商业目的。你并不需要公开所制作的游戏的源代码。     

### 中文文档
由个人汉化翻译，如有错误，请指正。    
mail:[chizuru@foxmail.com](chizuru@foxmail.com)
