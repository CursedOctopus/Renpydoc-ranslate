# Renpydoc-translate

#### 项目介绍
翻译Renpy官方文档到中文

参考[2015年由fk1995创建的项目](https://github.com/fk1995/RenPy-Documentation-translations)

#### 软件架构
网页框架由[amWiki](http://amwiki.org/)驱动，感谢amWiki作者

#### 最后更新时间
2018/5/5

#### 目前进度

2018/5/5    
Text, Displayables, Transforms, and Transitions
- Layered Images   
    -  Python   
  

2018/5/4    
Text, Displayables, Transforms, and Transitions
- Layered Images   
    -  Statement Reference    
    -  Poses   
    -  Advice    

2018/5/3
Text, Displayables, Transforms, and Transitions   
* Layered Images  
    -  Using an Layered Image  
    -  Automatic Attributes  


2018/4/25
Text, Displayables, Transforms, and Transitions   
- Layered Images  
    -  define layered images   


2018/4/23
GUI Customization Guide   
- Simple GUI Customization
- Intermediate GUI Customization
    -  Dialogue
    -  Choice Menus
    -  Overlay Images

2018/4/22
Quick Start     
   - Images
   - Transitions
   - Positions
   - Music and Sound
   - Pause Statement
   - Ending the Game
   - Menus, Labels, and Jumps
   - Supporting Flags using the Default, Python and If Statements
   - Releasing Your Game
   - Script of The Question
   - Where do we go from here?   
  
GUI Customization Guide
   - Simple GUI Customization

2018/4/21
Quick Start     
   - The Ren'Py Launcher
   - A Simple Game
   - Characters
